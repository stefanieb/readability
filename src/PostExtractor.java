import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.io.FileUtils;

import at.aau.steffi.utils.EArrayList;
import at.aau.steffi.utils.EList;

public class PostExtractor {

	private  String url = "jdbc:mysql://localhost:3306/";
	private  String dbName = "SO_Nov2014";
	private  String driver = "com.mysql.jdbc.Driver";
	private  String userName = "root";
	private  String password = "";
	
	public static void main(String args[]) throws IOException{
		PostExtractor pe = new PostExtractor();
		EList<StackOverflowPost> posts = pe.getMatrixPosts();

		pe.savePostAsTxt(posts);
	}
	
	
	public EList<StackOverflowPost> getMatrixPosts() {
		EList<StackOverflowPost> returnList = new EArrayList<>();
		try {
			Class.forName(driver).newInstance();
			Connection conn = DriverManager.getConnection(url + dbName, userName, password);
			Statement st = conn.createStatement();

			ResultSet tags_db = st.executeQuery(
					"select post_id, category, a.* from question_categories join aposts a on post_id = a.Id limit 0, 1500;");

			while (tags_db.next()) {

				StackOverflowPost post = new StackOverflowPost(tags_db.getInt("post_id"), tags_db.getString("Body"), tags_db.getString("Tags"), tags_db.getString("Title"));
				returnList.add(post);
			}
			conn.close();

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return returnList;
	}
	
	public void savePostAsTxt(EList<StackOverflowPost> posts) throws IOException{
		
		FileUtils.cleanDirectory(new File("data/")); 
		
		for(StackOverflowPost post: posts){
			
			try {
				FileWriter writer = new FileWriter("data/" + post.getId() + ".html", true);

				// header
				writer.append("<!DOCTYPE html><html><head><title>TEST</title></head><body>");
				writer.append(post.getTitle());
				writer.append("<br>");
				writer.append(post.getCleanBody());
				writer.append("</body></html>");
				

				writer.flush();
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
	}
	
}
