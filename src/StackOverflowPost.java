


public class StackOverflowPost {

	public StackOverflowPost(Integer id, String body, String tags, String title) {
		super();
		this.id = id;
		this.body = body;
		this.tags = tags;
		this.title = title;
	}

	private Integer id;
	private String body;
	private String tags;
	private String title;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getCleanBody(){
		
		String returnBody = "";
		returnBody = body.replaceAll("<code>(.+?)</code>", "");
//		returnBody = returnBody.replaceAll("<p>", "");
//		returnBody = returnBody.replaceAll("<pre>", "");
//		returnBody = returnBody.replaceAll("</pre>", "");
//		returnBody = returnBody.replaceAll("</p>", "");
//		returnBody = returnBody.replaceAll(";", "");
//		returnBody = returnBody.replaceAll("&#xA", "");
//		returnBody = returnBody.replaceAll("<a href=.*\">", "");
//		returnBody = returnBody.replaceAll("</a>", "");
//		returnBody = returnBody.replaceAll("<strong>", "");
//		returnBody = returnBody.replaceAll("</strong>", "");
//		returnBody = returnBody.replaceAll("<blockquote>", "");
//		returnBody = returnBody.replaceAll("</blockquote>", "");
//		returnBody = returnBody.replaceAll("<em>", "");
//		returnBody = returnBody.replaceAll("</em>", "");
//		returnBody = returnBody.replaceAll("<br>", "");
		
		
//		if(returnBody.contains("<")){
//			System.out.println(returnBody);
//		}
		
		return returnBody;
	}

}

